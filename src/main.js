import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store'
import App from './App.vue'

//Plugins
import BaseComponents from './plugins/baseComponents'

Vue.use(BaseComponents)

//Routes
import Stores from './modules/stores';
import Orders from './modules/orders';

const router = new VueRouter({
  routes: [
    {
      path: '/stores',
      component: Stores
    },
    {
      path: '/orders',
      component: Orders
    }
  ]
})

Vue.use(VueRouter)

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
