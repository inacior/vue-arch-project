export default class Order {

    constructor(initialData = {}) {
        const { 
            uuid, 
            created_at, 
            name, 
            gender, 
            status, 
            email, 
            address, 
            latitude, 
            longitude, 
            products } = initialData

        this.uuid = uuid;
        this.created_at = created_at;
        this.name = name;
        this.gender = gender;
        this.status = status;
        this.email = email;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.products = products || [];
    }

    get statusName () {
        const statusNames = {
            1: 'PENDING',
            2: 'COOKING',
            3: 'DELIVERED',
            4: 'CANCELED',
        }

        return statusNames[this.status] || '-'
    }
    get value () {
        return this.products.reduce((memo, product) => memo += (product.value || 0), 0.0)
    }

}
