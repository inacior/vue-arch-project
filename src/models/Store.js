export default class Store {

    constructor(initialData = {}) {
        const { 
            uuid,
            franchisee,
            address,
            latitude, 
            longitude,
            active } = initialData

        this.uuid = uuid;
        this.franchisee = franchisee;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.active = active;
    }

}
