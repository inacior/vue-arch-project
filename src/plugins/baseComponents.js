import BaseNav from '../components/BaseNav.vue'

const install = (Vue) => {

    Vue.component('base-nav', BaseNav);
    
}

export default { install }