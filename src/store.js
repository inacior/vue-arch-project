import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import orderModule from './modules/orders/_store';
import storeModule from './modules/stores/_store';

export default new Vuex.Store({
  modules: {
    orders: orderModule,
    stores: storeModule
  }
})