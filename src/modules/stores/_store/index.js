import _api from '../_api'

import Store from '@models/Store';

export default {
    namespaced: true,
    // -----------------------------------------------------------------
    state: {
        stores: []
    },
    // -----------------------------------------------------------------
    getters: {
        all: state => state.stores,
        count: state => {
            return state.stores.reduce((memo, store) => {
                if(store.active) memo.active += 1
                else memo.inactive += 1

                return memo
            }, {active: 0, inactive: 0})
        }
    },
    // -----------------------------------------------------------------
    mutations: {
        setStores: (state, stores) => state.stores = stores
    },
    // -----------------------------------------------------------------
    actions: {
        get: ({ commit }) => {
            return _api.get()
                .then(stores => {
                    stores = stores.map(store => new Store(store))

                    commit('stores/setStores', stores, { root: true })
                    
                    return stores
                })
        },
    }
}