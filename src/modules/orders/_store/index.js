import _api from '../_api'
import Order from '@models/Order'

export default {
    namespaced: true,
    // -----------------------------------------------------------------
    state: {
        orders: []
    },
    // -----------------------------------------------------------------
    getters: {
        all: state => state.orders,
        count: state => {
            return state.orders.reduce((memo, item) => {
                if(!memo[item.statusName]) memo[item.statusName] = 0;
                
                memo[item.statusName] += 1
                
                return memo
            }, {})
        }
    },
    // -----------------------------------------------------------------
    mutations: {
        setOrders: (state, orders) => state.orders = orders
    },
    // -----------------------------------------------------------------
    actions: {
        get: ({ commit }) => {
            return _api.get()
                .then(orders => {
                    orders = orders.map(order => new Order(order))

                    commit('orders/setOrders', orders, { root: true })
                    
                    return orders
                })
        },
    }
}