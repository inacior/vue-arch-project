import dumbData from './dumb.json'

const get = () => {

    return new Promise((resolve) => {

        setTimeout(() => {
            resolve(dumbData)
        }, 700);
        
    })

}

export default { get }